package kz.ospanov.exercise.configuration;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import kz.ospanov.exercise.dao.UserDAO;
import kz.ospanov.exercise.dao.UserDAOImpl;
import kz.ospanov.exercise.model.User;
import kz.ospanov.exercise.model.StatisticsInfo;
import kz.ospanov.exercise.model.StatisticsInfoRSP;
import kz.ospanov.exercise.model.UserStatusInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Front Controller class.
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 28, 2017
 */
@RestController
public class UserController {

    @Autowired
    private UserDAOImpl userDAO;

    @RequestMapping("/")
    public String welcome() {
        return "welcome to exercise...";
    }

    /**
     * получение статистики с сервера
     *
     * @param StatisticsInfo объект содержащий параметры для извлечения данных
     * по статистике пример запроса: {"requestID":"123","status":"Offline"}
     * @return StatisticsInfoRSP список пользоввателей и ID входяшего запроса
     * пример ответа: {"users":[{"pictureURI":
     * "https://test.com/pic3.jpeg","userName": "thirdUserName","email":
     * null,"status": "Offline","userID": null}], "requestID": "123" }
     * @exception - no exception propogations. В случае exception текст ошибки в
     * поле error.
     */
    @RequestMapping(value = "/user/getStatistics", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json")
    public ResponseEntity getStatistics(@RequestBody StatisticsInfo info) {
        try {
            System.out.println(info.getStatus() == null ? "performing statistics on all statuses" : "performing statistics by user status " + info.getStatus());
            List<User> usersInfo = userDAO.getStatistics(info);
            System.out.println("requestID " + info.getRequestID());
            System.out.println("done.....");
            return new ResponseEntity((info.getRequestID() != null ? new StatisticsInfoRSP(info.getRequestID(), usersInfo) : new StatisticsInfoRSP(usersInfo)), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("cannot form statistics info due this error: " + e);
            return new ResponseEntity<>(Collections.singletonMap("error", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Изменение статуса пользователя (Online, Offline).
     *
     * @param User объект содержащий параметры пользователя, в частности userID
     * пример запроса: {"userID":"AB-101","status":"Online"}
     * @return UserStatusInfo. Объект содержит уникальный ID пользователя, новый
     * и предыдущий статус пример ответа
     * {"oldStatus":"Offline","newStatus":"Online","userID":"AB-101"}
     * @exception - no exception propogations. В случае exception текст ошибки в
     * поле error.
     */
    @RequestMapping(value = "/user/changestatus", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json")
    public ResponseEntity changeUserStatus(@RequestBody User newUser) {
        try {
            System.out.println("changing user status with ID: " + newUser.getUserID());
            String oldStatus = userDAO.changeUserStatus(newUser);
            System.out.println("done.....");
            return new ResponseEntity(new UserStatusInfo(newUser.getUserID(), oldStatus, newUser.getStatus()), HttpStatus.OK);
        } catch (org.springframework.dao.EmptyResultDataAccessException erdae) {
            return new ResponseEntity<>(Collections.singletonMap("error", "No user was found by ID" + newUser.getUserID()), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("cannot change user status due to this exception: " + e);
            return new ResponseEntity<>(Collections.singletonMap("error", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Получение информации о пользователе
     *
     * @param User объект содержащий параметры пользователя, в частности userID
     * пример запроса: {"userID":"AB-104"}
     * @return User. Объект содержит информацию по пользователю пример ответа:
     * {"pictureURI":"https://test.com/pic4.jpeg","userName":"forthUserName","email":"test4@test.com","status":"Offline","userID":"AB-104"}
     * @exception - no exception propogations. В случае exception текст ошибки в
     * поле error.
     */
    @RequestMapping(value = "/user/getinfo", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json")
    public ResponseEntity getUserInfoByID(@RequestBody User newUser) {
        try {
            System.out.println("searching user by ID: " + newUser.getUserID());
            User user = userDAO.getUserByID(newUser.getUserID());
            System.out.println("done.....");
            return new ResponseEntity(user, HttpStatus.OK);
        } catch (org.springframework.dao.EmptyResultDataAccessException erdae) {
            return new ResponseEntity<>(Collections.singletonMap("error", "No user was found by ID" + newUser.getUserID()), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("cannot extract user info due to this exception: " + e);
            return new ResponseEntity<>(Collections.singletonMap("error", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Создание нового пользователя
     *
     * @param User объект содержащий параметры пользователя (URI картинки, имя
     * пользователя, email и т.д.). пример запроса:
     * {"pictureURI":"https://test.com/pic4.jpeg","userName":"forthUserName","email":"test4@test.com","status":"Offline","userID":"AB-104"}
     * @return String. уникальный ID нового пользователя. {"userID": "AB-104"}
     * @exception. Исключения не выбрасывает, вместо него возвращает текст
     * ошибки в поле error.
     *
     */
    @RequestMapping(value = "/user/create", method = {RequestMethod.POST, RequestMethod.GET}, produces = "application/json")
    public ResponseEntity createUser(@RequestBody User newUser) {
        try {
            System.out.println("creating new User. ID: " + newUser.getUserID());
            userDAO.createUser(newUser);
            System.out.println("new User with ID" + newUser.getUserID() + " successfully created...");
            return new ResponseEntity(Collections.singletonMap("userID", newUser.getUserID()), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("cannot create new user due to this exception: " + e);
            return new ResponseEntity<>(Collections.singletonMap("error", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
