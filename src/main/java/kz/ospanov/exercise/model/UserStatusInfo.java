package kz.ospanov.exercise.model;

/**
 * POJO класс для отображения изменения статуса пользователя
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
public class UserStatusInfo {

    private String oldStatus;
    private String newStatus;
    private String userID;

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public UserStatusInfo() {
    }

    public UserStatusInfo(String userID, String oldStatus, String newStatus) {
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
        this.userID = userID;
    }

}
