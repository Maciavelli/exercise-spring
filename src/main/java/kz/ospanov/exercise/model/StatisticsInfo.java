package kz.ospanov.exercise.model;

/**
 * POJO класс для отображения параметров получения статистики пользователей
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
public class StatisticsInfo {

    private String requestID;
    private String status;

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StatisticsInfo() {
    }

}
