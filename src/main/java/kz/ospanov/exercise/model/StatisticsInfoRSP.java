package kz.ospanov.exercise.model;

import java.util.List;

/**
 * POJO класс для отображения статистика пользователей
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
public class StatisticsInfoRSP {

    private List<User> users;
    private String requestID;

    public StatisticsInfoRSP(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public StatisticsInfoRSP() {
    }

    public StatisticsInfoRSP(String requestID, List<User> users) {
        this.users = users;
        this.requestID = requestID;
    }

}
