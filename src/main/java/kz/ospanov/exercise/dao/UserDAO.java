package kz.ospanov.exercise.dao;

import java.util.List;
import kz.ospanov.exercise.model.User;
import org.springframework.stereotype.Repository;

/**
 * DAO интерфейс 
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
public interface UserDAO {
     public List<User> queryAllUsers();
}
