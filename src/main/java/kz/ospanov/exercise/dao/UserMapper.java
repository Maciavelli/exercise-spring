package kz.ospanov.exercise.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import kz.ospanov.exercise.model.User;
import org.springframework.jdbc.core.RowMapper;

/**
 * Mapper класс для обработки resultSet
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
public  class UserMapper implements RowMapper<User> {

    
        public User mapRow(ResultSet resulSet, int rowNum) throws SQLException {
            User user = new User();
            user.setUserID(resulSet.getString("USERID"));
            user.setUserName(resulSet.getString("USERNAME"));
            user.setEmail(resulSet.getString("EMAIL"));
            user.setStatus(resulSet.getString("STATUS"));
            user.setPictureURI(resulSet.getString("PICTUREURI"));

            return user;
        }
    }
