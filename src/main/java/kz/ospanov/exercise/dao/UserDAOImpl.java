/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.ospanov.exercise.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import kz.ospanov.exercise.model.User;
import kz.ospanov.exercise.model.StatisticsInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * DAO class для работы с СУБД
 *
 * @author Ospanov Kuat
 * @version 1.0 Jul 30, 2017
 */
@Repository
public class UserDAOImpl {

    private final static String STAT_SQL = "select username,status,pictureuri from users ";
    private final static String USERINFO_SQL = "select * from users where userid=?";
    private final static String UPDATE_STATUS_SQL = "update users set status=? where userid=?";
    private final static String INSERT_USERINFO_SQL = "insert into users values(?,?,?,?,?)";
    @Autowired
    DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void init() {
        System.out.println("UserDAOImpl postConstruct is called. datasource = " + dataSource);
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * получение статистики с сервера
     *
     * @param StatisticsInfo объект содержащий параметры для извлечения данных
     * @return List<User> список пользователей
     */
    public List<User> getStatistics(StatisticsInfo info) {
        System.out.println("SQLDAO: getStatistics is called");
        List<User> userList = this.jdbcTemplate.query((info.getStatus() == null ? STAT_SQL : STAT_SQL.concat("where status=?")), info.getStatus() == null ? null : new String[]{info.getStatus()}, new RowMapper<User>() {
            public User mapRow(ResultSet resulSet, int rowNum) throws SQLException {
                User user = new User();
                user.setUserName(resulSet.getString("USERNAME"));
                user.setStatus(resulSet.getString("STATUS"));
                user.setPictureURI(resulSet.getString("PICTUREURI"));
                return user;
            }
        });
        System.out.println("query done...");
        return userList;
    }

    /**
     * Получение информации о пользователе
     *
     * @param String. ID пользователя
     * @return User. Объект содержит информацию по пользователю пример ответа:
     */
    public User getUserByID(String id) {
        System.out.println("SQLDAO: user searching is called...");
        // final String QUERY_SQL = "select * from users where userid=?";
        User user = this.jdbcTemplate.queryForObject(USERINFO_SQL, new String[]{id}, new UserMapper());
        System.out.println("query done...");
        return user;
    }

    /**
     * Изменение статуса пользователя (Online, Offline).
     *
     * @param User объект содержащий параметры пользователя
     * @return String. Предыдущий статус
     */
    public String changeUserStatus(User newUser) {
        System.out.println("SQLDAO: user status changing is called...");
        String oldStatus = this.getUserByID(newUser.getUserID()).getStatus();
        // final String UPDATE_SQL = "update users set status=? where userid=?";
        System.out.println("Updated Record with ID = " + newUser.getUserID());
        this.jdbcTemplate.update(UPDATE_STATUS_SQL, newUser.getStatus(), newUser.getUserID());
        System.out.println("update query done...");
        return oldStatus;

    }

    /**
     * Создание нового пользователя
     *
     * @param User объект содержащий параметры пользователя (URI картинки, имя
     * пользователя, email и т.д.).
     * @return Nothing
     */
    public void createUser(User newUser) {
        System.out.println("SQLDAO: userCreateing script is called...");
        this.jdbcTemplate.update(INSERT_USERINFO_SQL, newUser.getUserID(), newUser.getUserName(), newUser.getEmail(), newUser.getStatus(), newUser.getPictureURI());
        System.out.println("new user recored created. UserID = " + newUser.getUserID() + " userName = " + newUser.getUserName() + " email = " + newUser.getEmail() + " status = " + newUser.getStatus() + " pictureURI = " + newUser.getPictureURI());

    }
}
