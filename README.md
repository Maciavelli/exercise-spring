# тренировочное управжнения для кандидата #

Данный репозитарий содержит maven проект, который реализует RESTful интерфейс с использованием фреймворка Spring 4.
В качестве СУБД использовалась PostgreSQL, так как она была под рукой. Но в целом, в данном проекте не имеет принципиального значения так как в DAO 
слое заложены ANSI(SQL-92) SQL команды которые будут работать на любой реляционной БД.
В качестве сервера приложений использовался WildFly 10 final, это то, что оказалось под рукой и наиболее мне удобно в виду большого опыта работы с ним.


### Сборка и настройка ###
для сборки проект используется команда
```
#!bash

mvn clean install
```
для дейплоймента удобнее всего воспользоваться WildFly плагином

```
#!bash

mvn wildfly:deploy
```
Предварительно необходимо создать таблицы в базе данных. Ниже DDL скрипт:


```
#!sql

CREATE TABLE users
(
  userid character varying(80) NOT NULL, -- ID пользователя
  username character varying(80), -- имя пользователя
  email character varying(80), -- email пользователя
  status character varying(20), -- статус - online/offline
  pictureuri character varying(80), -- URI картинки пользователя
  CONSTRAINT users_pkey PRIMARY KEY (userid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;
COMMENT ON COLUMN users.userid IS 'ID пользователя';
COMMENT ON COLUMN users.username IS 'имя пользователя';
COMMENT ON COLUMN users.email IS 'email пользователя';
COMMENT ON COLUMN users.status IS 'статус - online/offline';
COMMENT ON COLUMN users.pictureuri IS 'URI картинки пользователя';
```

Проект протестирован силами разработчика. Примеры запросов
### Получение информации о пользователе ### 
запрос
```json
{"userID":"AB-104"}
```
ответ
```json
{
    "pictureURI": "https://test.com/pic4.jpeg",
    "userName": "forthUserName",
    "email": "test4@test.com",
    "status": "Online",
    "userID": "AB-104"
}
```
###  Статистика сервера ### 
запрос:
```json
{
"status":"Offline","requestID":"asd"
}
```
ответ
```json
{
    "users": [
        {
            "pictureURI": "https://test.com/pic3.jpeg",
            "userName": "thirdUserName",
            "email": null,
            "status": "Offline",
            "userID": null
        },
        {
            "pictureURI": "https://test.com/pic4.jpeg",
            "userName": "forthUserName",
            "email": null,
            "status": "Offline",
            "userID": null
        }
    ],
    "requestID": "asd"
}
```
###  создание пользователя ### 
запрос
```json
{"pictureURI":"https://test.com/pic3.jpeg","userName":"thirdUserName","email":"test3@test.com","status":"Offline","userID":"AB-555"}
```
ответ
```json
{
    "userID": "AB-555"
}
```
###  изменение статуса пользователя ### 
запрос
```json
{"userID":"AB-101",
"status":"Online"
}
```
Ответ
```json
{
    "oldStatus": "Online",
    "newStatus": "Online",
    "userID": "AB-101"
}
```
### Пример ошибочного запроса: ### 
```json
{"userID":"AB-404",
"status":"Online"
}
```
ответ
```json
{
    "error": "No user was found by IDAB-404"
}
```